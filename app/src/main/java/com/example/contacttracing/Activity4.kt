package com.example.contacttracing

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

public class Activity4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_four)

        setTitle("Confirmed")
    }
}