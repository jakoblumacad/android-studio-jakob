package com.example.contacttracing

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Activity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.page_two)

        this.setTitle("Health Information")

        val Symptoms = findViewById<TextView>(R.id.symptoms)
        val History = findViewById<TextView>(R.id.history)

        findViewById<Button>(R.id.button2).setOnClickListener {

            val entry1 = intent.getStringExtra("name").toString()
            val entry2 = intent.getStringExtra("number").toString()
            val entry3 = intent.getStringExtra("residence").toString()
            val entry4 = intent.getStringExtra("symtoms").toString()
            val entry5 = intent.getStringExtra("symptons").toString()


            val intent = Intent(this, Activity3::class.java)
            intent.putExtra("name", entry1)
            intent.putExtra("number", entry2)
            intent.putExtra("residence", entry3)
            intent.putExtra("symptoms", Symptoms.text.toString())
            intent.putExtra("history", History.text.toString())
            startActivity(intent)
        }
    }
}
