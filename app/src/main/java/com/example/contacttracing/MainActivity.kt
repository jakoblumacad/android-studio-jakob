package com.example.contacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?)
        {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            this.setTitle("Guest Information")
            val Fullname = findViewById<TextView>(R.id.name)
            val MobileNumber = findViewById<TextView>(R.id.number)
            val City = findViewById<TextView>(R.id.residence)

            findViewById<Button>(R.id.button1).setOnClickListener {
                Toast.makeText(applicationContext, Fullname.text, Toast.LENGTH_LONG).show()
                val intent = Intent(this, Activity2::class.java)
                intent.putExtra("Fullname", Fullname.text.toString())
                intent.putExtra("MobileNumber", MobileNumber.text.toString())
                intent.putExtra("City", City.text.toString())
                startActivity(intent)
            }

        }
}